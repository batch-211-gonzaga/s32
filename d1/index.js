// Node js routing

const http = require('http');

const PORT = 4000

http.createServer((request, response) => {
  if (request.url == '/items' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Data retrieved from the database');
  }

  if (request.url == '/items' && request.method == 'POST') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end('Data to be sent to the database')
  }
}).listen(PORT);

console.log(`Server running at localhost:${PORT}`);
