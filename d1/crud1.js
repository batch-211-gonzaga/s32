const http = require('http');

let directory = [
  {
    name: 'Giovanni',
    email: 'giovanni@mail.com'
  },
  {
    name: 'Johari',
    email: 'johari@mail.com'
  }
]

http.createServer(function (request, response) {
  if (request.url == '/users' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.write(JSON.stringify(directory));
    response.end();
  }

  if (request.url == '/users' && request.method == 'POST') {
    // 1. Declare a placeholder
    let request_body = '';

    // 2. When new data, assign data to empty request_body
    // data | error | end
    request.on('data', function (data) {
      request_body += data;
    });

    // 3. Before request ends, add new data to existing users array
    request.on('end', function () {
      console.log(typeof request_body);

      // 4. Convert request_body from JSON to JS object then assign back to request_body
      request_body = JSON.parse(request_body);

      // 5. Declare new directory var with new data from postman
      let new_directory = {
        name: request_body.name,
        email: request_body.email
      }

      // 6. Add new_directory to users
      directory.push(new_directory);
      console.log(directory)

      // 7. Wreite headers for response
      response.writeHead(200, { 'Content-Type': 'application/json' });
      response.write(JSON.stringify(new_directory));
      response.end();
    });
  }
}).listen(4000);

console.log('Server running at localhost:4000');
