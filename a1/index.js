const http = require('http');

const PORT = 4000;

http.createServer((request, response) => {
  if (request.url == '/') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('📚 Welcome to Booking System™️');
  } else if (request.url == '/profile') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('👤 Welcome to your profile!');
  } else if (request.url == '/courses') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('💡 Here\'s our courses available');
  } else if (request.url == '/addcourse') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('➕ Add a course to our resources.');
  } else if (request.url == '/updatecourse') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('⬆️  Update a course to our resources.');
  } else if (request.url == '/archivecourses') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('🗄️ Archive courses to our resources.');
  }
}).listen(PORT);

console.log(`Server running. Access at [http://localhost:${PORT}]`);
